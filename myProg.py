#!/bin/python

def getMaxElement(entryList=None):
    return min(entryList)


def main():
    entryList = [2, 1, 5, 4, 16, 90, 40]
    
    print(getMaxElement(entryList))


if __name__ == "__main__":
    main()